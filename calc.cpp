#include "calc.h"
#include "ui_calc.h"

Calc::Calc(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Calc)
{
    ui->setupUi(this);
}

Calc::~Calc()
{
    delete ui;
}


void Calc::on_add_clicked()
{
    float a = ui->first_number->value();
    float b = ui->second_number->value();
    float result = a + b;
    ui->result->setText(QString::number(result));
}

void Calc::on_subtract_clicked()
{
    float a = ui->first_number->value();
    float b = ui->second_number->value();
    float result = a - b;
    ui->result->setText(QString::number(result));

}

void Calc::on_multiply_clicked()
{
    float a = ui->first_number->value();
    float b = ui->second_number->value();
    float result = a * b;
    ui->result->setText(QString::number(result));

}

void Calc::on_divide_clicked()
{
    float a = ui->first_number->value();
    float b = ui->second_number->value();
    float result = a / b;
    ui->result->setText(QString::number(result));

}
