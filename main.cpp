#include "calc.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Calc w;
    a.setStyle("fusion");
    w.show();
    return a.exec();
}
