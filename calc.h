#ifndef CALC_H
#define CALC_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Calc; }
QT_END_NAMESPACE

class Calc : public QMainWindow
{
    Q_OBJECT

public:
    Calc(QWidget *parent = nullptr);
    ~Calc();

private slots:
    void on_add_clicked();

    void on_subtract_clicked();

    void on_multiply_clicked();

    void on_divide_clicked();

private:
    Ui::Calc *ui;
};
#endif // CALC_H
